package com.beweb.montpellier.poo.entites_non_vivantes.exercices.one;
import com.beweb.montpellier.poo.entites_non_vivantes.exercices.one.Factory;

public class Main{
    public static void main(String[] args){
        System.out.println(" \n"); // test lancement
        
        Factory.factory(); // crée des meubles et affiche une description      
        
        Personne tony=new Personne("Tony", "Truand"); // crée une personne 
        
        Meuble table=Factory.factory("table"); // crée une table 
        tony.observer(table); 
        
        
        Meuble armoire=Factory.factory("armoire"); // crée une armoire
        tony.observer(armoire);


        Meuble chaise=Factory.factory("chaise"); // crée une chaise
        tony.observer(chaise);

        Meuble crashTest= Factory.factory("   ygygiuhjio  "); // crashtest valeur par defaut
        tony.observer(crashTest);
    }
}
