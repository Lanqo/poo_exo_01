package com.beweb.montpellier.poo.entites_non_vivantes.exercices.one;
import java.util.List;
import java.util.ArrayList;

public class Chaise extends Meuble {
    private Siege siege;                         // la chaise a un siège, un dossier, et une case 
    private Dossier dossier;
    private Case assisOrNotAssis;

    public Chaise(Meuble meuble){                // crée une chaise à partir d'un meuble
        super(meuble.lpied, meuble.matiere);
        this.siege= new Siege();                 // crée un Siège et un dossier de dimension aléatoire
        this.dossier= new Dossier();
        if ((int)(Math.random()*2)<1){                                   // décide aléatoirement s'il y a un poisson assis sur la chaise ou si elle est vide
            this.assisOrNotAssis=new Case("avec un poisson assis, ");
        }
        else {
            this.assisOrNotAssis=new Case();
        }

    }

    public Chaise(Dossier doss, Siege poseFesse, Matiere mat, List<Pied> listPied){     // Second constructeur
        this.matiere=mat;
        this.lpied= listPied;
        this.siege=poseFesse;
        this.dossier=doss;
        if ((int)(Math.random()*2)<1){
            this.assisOrNotAssis=new Case("avec un poisson assis, ");
        }
        else {
            this.assisOrNotAssis=new Case();
        }
    } 
    
    public Chaise(Dossier doss, Siege poseFesse, Matiere mat){     // Second constructeur
        this.matiere=mat;
        this.lpied= new ArrayList<>();
        Pied pied=new Pied();
        for (int i=0; i<4;i++){
            this.lpied.add(pied);
        }
        this.siege=poseFesse;
        this.dossier=doss;
        if ((int)(Math.random()*2)<1){
            this.assisOrNotAssis=new Case("avec un poisson assis, ");
        }
        else {
            this.assisOrNotAssis=new Case();
        }
    }
    
    public Chaise(Dossier doss, Siege poseFesse, Meuble meub){ // troisième constructeur

        this.matiere=meub.matiere;
        this.lpied=meub.lpied;
        this.siege=poseFesse;
        this.dossier=doss;
        if ((int)(Math.random()*2)<1){
            this.assisOrNotAssis=new Case("avec un poisson assis,");
        }
        else {
            this.assisOrNotAssis=new Case();
        }
    }  

    @Override
    public String contenu(){  // retourne le contenu (poisson assis ou vide)
        return "je suis une chaise " + this.assisOrNotAssis.contenu() ;
    }
}