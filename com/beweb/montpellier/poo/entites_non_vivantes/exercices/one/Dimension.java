package com.beweb.montpellier.poo.entites_non_vivantes.exercices.one;

/**
 * Dimension
 */
public class Dimension {
    private int  largeur;
    private int longueur;
    private int hauteur;
    
    protected Dimension(){
        this.hauteur=(int)(Math.random()*100);  // affecte une entier aleatoire de 0 à 99 à la hauteur
        this.largeur=(int)(Math.random()*100);
        this.longueur=(int)(Math.random()*100);
    }
    
    protected Dimension(int larg, int lon,int haut){
        this.largeur= larg;
        this.longueur= lon;
        this.hauteur= haut;
    }

    public int getLarg(){
        return largeur;
    }
    public int getLong(){
        return longueur;
    }
    public int getHaut(){
        return hauteur;
    }
    public void setLarg(int larg){
        this.largeur=larg;
    }
    public void setLong(int lon){
        this.longueur=lon;
    }
    public void setHaut(int haut){
        this.hauteur=haut;
    }
}