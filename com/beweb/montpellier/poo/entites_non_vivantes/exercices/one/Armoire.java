package com.beweb.montpellier.poo.entites_non_vivantes.exercices.one;
import com.beweb.montpellier.poo.entites_non_vivantes.exercices.one.Case;
import java.util.List;
import java.util.ArrayList;

public class Armoire extends Meuble {  // crée une armoire (meuble contenant plusieurs cases)
    private List<Case> cases;         
    private Dimension dim;

    public Armoire(){
        super();
        this.dim=new Dimension();
        this.cases= new ArrayList<>();
    }

    public Armoire(List<Case> lcase, List<Pied> listPied, Matiere mat, Dimension dime){  // 
        super(listPied,mat);
        this.cases= lcase;
        this.dim=dime;
    }

    public Armoire(List<Case> lcase, Meuble meub){
        this.cases= lcase;
        this.lpied=meub.lpied;
        this.matiere=meub.matiere;
    }
    

    @Override
    public String contenu(){  // retourne le contenu de l'armoire (de toutes les cases de la liste de case)
        String cont="Je suis une armoire contenant";
        for(int i=0;i<cases.size();i++){
            cont=cont + cases.get(i).contenu();
        }
        return cont ;
    }
}
