package com.beweb.montpellier.poo.entites_non_vivantes.exercices.one;
import com.beweb.montpellier.poo.entites_non_vivantes.exercices.one.Pied;
import com.beweb.montpellier.poo.entites_non_vivantes.exercices.one.Matiere;
import java.util.ArrayList;
import java.util.List;
public class Meuble extends Case{
    protected List<Pied> lpied;
    protected Matiere matiere;

    public Meuble (List<Pied> listPied, Matiere mat){
        this.lpied=listPied;
        this.matiere=mat;
    }

    public Meuble (){
        this.lpied= new ArrayList<>();
        this.matiere=new Matiere();
    }

    public Meuble (String couleurMeuble, String nomMatiere, int taillePied, int nombrePied){
        this.matiere= new Matiere(couleurMeuble,nomMatiere);
        this.lpied=new ArrayList<>();
        for (int i=0;i<nombrePied;i++){
            this.lpied.add(new Pied(taillePied));
        }
    }

    @Override
    public String toString(){
        if (this.lpied.isEmpty()){   
            if (matiere.getNom()==""){
                return " je n'ai pas de pied et je suis composé de rien, en fait je n'existe pas vraiment et je suis simplement un test" + "\n"; 
            }
            else {
                return " je n'ai pas de pied et je suis composé de " + this.matiere.getNom() + " de couleur " + this.matiere.getCouleur() +"\n";
            }
        }
        else {
            return " j'ai "+ String.valueOf(this.lpied.size()) + " pieds de " + this.lpied.get(0).getHauteur()+ " cm de hauteur et je suis composé de " + this.matiere.getNom() + " de couleur " + this.matiere.getCouleur() + "\n";
        }
    }
}