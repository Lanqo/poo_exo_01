package com.beweb.montpellier.poo.entites_non_vivantes.exercices.one;

public class Dossier extends Dimension{

    public Dossier(){
        this.setLarg(0);
        this.setHaut(0);
        this.setLong(0);
    }

    public Dossier(int lar, int lon, int haut){
        this.setLarg(lar);
        this.setLong(lon);
        this.setHaut(haut);
    }

    public Dossier(Dimension dim){
        this.setLarg(dim.getLarg());
        this.setLong(dim.getLong());
        this.setHaut(dim.getHaut());
    }
}

