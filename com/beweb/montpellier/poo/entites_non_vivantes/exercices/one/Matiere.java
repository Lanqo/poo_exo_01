package com.beweb.montpellier.poo.entites_non_vivantes.exercices.one;
public class Matiere{
    private String nom;
    private String couleur;

    public Matiere(){
        this.nom="";
        this.couleur="";
    }

    public Matiere(String n,String c){
        this.nom=n;
        this.couleur=c;
    }

    public String getNom(){
        return nom;
    }

    public String getCouleur(){
        return couleur;
    }

    public void setNom(String name){
        this.nom=name;
    }

    public void setCouleur(String coul){
        this.couleur=coul;
    }
}