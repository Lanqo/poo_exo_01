package com.beweb.montpellier.poo.entites_non_vivantes.exercices.one;
import com.beweb.montpellier.poo.entites_non_vivantes.exercices.one.Meuble;
import com.beweb.montpellier.poo.entites_non_vivantes.exercices.one.Armoire;
import com.beweb.montpellier.poo.entites_non_vivantes.exercices.one.Table;

import java.util.List;
import java.util.ArrayList;
public class Factory{
    public static void factory() {  // crée une table et une armoire et les affiche
        
        Matiere diamant= new Matiere("diamant", "turquoise");  // crée la matière diamant turquoise 
        Pied pied= new Pied();                                 // crée un pied d'une taille aléatoire (de 10 à 110 cm)
        
        List<Pied> list2pied= new ArrayList<>();               // initialise une liste de pieds
        for(int i=0;i<(int)((Math.random()*10));i++){          // ajoute de 0 à 9 pieds à la liste de pieds
            list2pied.add(pied);                      
        }
        Meuble meuble= new Meuble(list2pied, diamant);         // crée un nouveau meuble fait de la liste de pied et du diamant turquoise
        Table table= new Table(meuble);                        // crée une table à partir du meuble précédent, avec une longueur/largeur aléatoire (de 0 à 100) et un contenu aléatoire
        table.afficheTable();                                  // affiche une description de la table 

        List<Case> lCases=new ArrayList<>();                   // crée une liste de cases
        for (int i=0;i<2;i++){                               
            lCases.add(new Case((int)(Math.random()*50)));     // ajoute jusqu'a 2 cases à la liste
        }
        Meuble meub=new Meuble();                                  // crée une nouveau meuble par défaut
        Armoire armoire= new Armoire(lCases,meub);                 // crée une armoire
        System.out.println(armoire.contenu()+armoire.toString());  // affiche une description de l'armoire et son contenu
    }

    public static Meuble factory(String typeMeuble)  {             // prend une chaine de caractere et crée au choix une table, une armoire ou une chaise. 
                                                                  // Retourne un meuble par defaut si mauvaise entrée
        Matiere carton= new Matiere("carton", "flashy");         // crée une matière 
        Pied pied= new Pied();                                    // crée un pied de hauteur aléatoire
        List<Pied> list2pied= new ArrayList<>();                  // initialise une liste de pied
        int j=((int)(Math.random()*6));                           // crée un entier aléatoire "j" de 1 à 5 
        for(int i=0;i<j;i++){                                     // ajoute de 0 à 5 fois le pied créée à la liste 
            list2pied.add(pied); 
        }
        Meuble meuble= new Meuble(list2pied, carton);            // crée un meuble avec la liste de pied et la matiere


        switch (typeMeuble){
            case "table":
                Table table=new Table(new Dimension(),meuble);        // crée une table avec une dimension aléatoire et le meuble précédent
                meuble=table;                                         // la variable meuble stocke la table
                return meuble ;                                       // renvoie meuble
            case "armoire":
                List<Case> lCases=new ArrayList<>();                  // crée une list de case
                for (int i=0;i<2;i++){                                // ajoute de 0 à 2 cases avec un contenu aléatoire à la liste de case
                    lCases.add(new Case((int)(Math.random()*50)));
                }
                Armoire armoire= new Armoire(lCases,meuble);          // crée une armoire a partir du meuble et de la liste de cases 
                meuble=armoire;
                return meuble;
            case "chaise":
                Chaise chaise=new Chaise(meuble);                     // crée une chaise à partir du meuble
                meuble=chaise;
                return meuble;
            default:                                                  // par defaut renvoie le meuble
                return meuble;
        }
    }
}