package com.beweb.montpellier.poo.entites_non_vivantes.exercices.one;

public class Personne {
    private String nom;
    private String prenom;

    public Personne(){
        this.nom="";
        this.prenom="";
    }

    public Personne(String pren, String name){
        this.nom=name;
        this.prenom=pren;
    }

    public Personne(String name){
        this.nom=name;
    }

    public void observer(Meuble meuble){
        System.out.println(meuble.contenu() + meuble.toString());
    }
}