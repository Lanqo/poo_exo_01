package com.beweb.montpellier.poo.entites_non_vivantes.exercices.one;
public class Pied{
    private int hauteur;
    
    public Pied(){
        this.hauteur=((int)(Math.random()*10))*10+10;
    }

    public Pied(int h){
        this.hauteur=h;
    }

    public int getHauteur(){
        return hauteur;
    }
}