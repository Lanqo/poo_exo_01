package com.beweb.montpellier.poo.entites_non_vivantes.exercices.one;
public class Case{
    private String contenu; // chaque case a un contenu

    public Case(){  // le constructeur par defaut crée une case vide
        this.contenu="vide,";  
    }

    public Case(String cont){  // constructeur dans lequel on entre directement le contenu de la case
        this.contenu=cont;
    }

    public Case(int a){  // contructeur qui determine un contenu aléatoire à la case à partir d'un entier (4 possibilités)
        a=a%4+1;
        switch (a) {
            case 1:
                this.contenu=" 2 pommes,";
                break;
            case 2:
                this.contenu=" un tonneau de rhum,";
                break;
            case 3:
                this.contenu=" le graal,";
                break;
            case 4: 
                this.contenu= " le one piece,";
                break;
            default:
                break;
        }
    }

    public String contenu(){  // retourne le contenu de la case
        return contenu;
    }
}