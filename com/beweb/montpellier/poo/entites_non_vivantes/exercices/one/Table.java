package com.beweb.montpellier.poo.entites_non_vivantes.exercices.one;
import java.util.List;
import com.beweb.montpellier.poo.entites_non_vivantes.exercices.one.Case;

public class Table extends Meuble{
    private Dimension dim;
    private Case contenu;

    public Table(){
        super(); 
        this.dim= new Dimension();  // Dimension aléatoire
        if (this.lpied.size()>0){
            this.dim.setHaut(this.lpied.get(0).getHauteur()+10); // affecte la hauteur des pied + 10CM à la hauteur de la table s'il y a au moins 1 pied
        }
        this.contenu=new Case(); // crée une case vide (ce qu'il y a sur la table)
    }

    public Table(Dimension xxx, List<Pied> listPied, Matiere mat, Case cont){
        this.dim=xxx; 
        
        this.lpied=listPied;
        this.matiere=mat;
        this.contenu=cont;
        if (this.lpied.size()>0){
            this.dim.setHaut(this.lpied.get(0).getHauteur()+10);
        }
    } 
    
    public Table(Dimension xxx, Meuble meub){
        this.dim=xxx;
        if (meub.lpied.size()>0){
            this.dim.setHaut(meub.lpied.get(0).getHauteur()+10);
        }
        this.lpied=meub.lpied;
        this.matiere=meub.matiere;
        this.contenu= new Case(xxx.getLarg());  // crée une case avec un contenu aléatoire (déterminé à partir de la largeur de la table)
    }
    
    public Table(Meuble meub){
        this.dim=new Dimension();
        if (meub.lpied.size()>0){
            this.dim.setHaut(meub.lpied.get(0).getHauteur()+10);
        }
        this.lpied=meub.lpied;
        this.matiere=meub.matiere;
        this.contenu= new Case(this.dim.getHaut());
    }
    
    public void afficheTable(){   // affiche une description de la table
        System.out.println("je suis une table de dimension "+ String.valueOf(dim.getLong()) +"x"+String.valueOf(dim.getLarg())+"x"+String.valueOf(dim.getHaut())+" cm, contenant " 
        +this.contenu.contenu() +toString());
    }

    @Override
    public String contenu(){ // retourne le contenu de la table
        return "je suis une table contenant"+ this.contenu.contenu();
    }
}