package com.beweb.montpellier.poo.entites_vivantes.exercice.one;
public class Eleve {
    String nom;
    String prenom;

    public Eleve(){
        this.nom="";
        this.prenom="";
    }
    
    /**
     * 
     * @param n
     * @param p
     */
    public Eleve(String n, String p){
        this.nom=n;
        this.prenom=p;
    }

    public void afficheEleve(){
        System.out.println("prenom: "+prenom);
        System.out.println("nom: "+nom);
    }
}